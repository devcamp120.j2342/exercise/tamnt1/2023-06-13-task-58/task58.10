package com.devcamp.voucher.services;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.devcamp.voucher.models.Voucher;
import com.devcamp.voucher.respository.PizzaRepository;
import com.devcamp.voucher.respository.VoucherRepository;

@Service
public class VoucherService {
        private final PizzaRepository pizzaRepository;
        private final VoucherRepository voucherRepository;

        public VoucherService(PizzaRepository pizzaRepository, VoucherRepository voucherRepository) {
                this.pizzaRepository = pizzaRepository;
                this.voucherRepository = voucherRepository;
        }

        public void createVouchers() {
                Date currentDate = new Date();

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                Voucher voucher1 = new Voucher("FA57A", "10", "", dateFormat.format(currentDate),
                                dateFormat.format(currentDate));
                Voucher voucher2 = new Voucher("TJ57A", "20", "", dateFormat.format(currentDate),
                                dateFormat.format(currentDate));
                Voucher voucher3 = new Voucher("TG57A", "40", "", dateFormat.format(currentDate),
                                dateFormat.format(currentDate));
                Voucher voucher4 = new Voucher("TA57A", "50", "", dateFormat.format(currentDate),
                                dateFormat.format(currentDate));
                Voucher voucher5 = new Voucher("TAS7A", "70", "", dateFormat.format(currentDate),
                                dateFormat.format(currentDate));
                Voucher voucher6 = new Voucher("TFJ7A", "10", "", dateFormat.format(currentDate),
                                dateFormat.format(currentDate));
                Voucher voucher7 = new Voucher("TJDDA", "10", "", dateFormat.format(currentDate),
                                dateFormat.format(currentDate));
                Voucher voucher8 = new Voucher("TJDAA", "10", "", dateFormat.format(currentDate),
                                dateFormat.format(currentDate));
                Voucher voucher9 = new Voucher("AFBDA", "10", "", dateFormat.format(currentDate),
                                dateFormat.format(currentDate));
                Voucher voucher10 = new Voucher("DAEDA", "10", "", dateFormat.format(currentDate),
                                dateFormat.format(currentDate));

                pizzaRepository.save(voucher1);
                pizzaRepository.save(voucher2);
                pizzaRepository.save(voucher3);
                pizzaRepository.save(voucher4);
                pizzaRepository.save(voucher5);
                pizzaRepository.save(voucher6);
                pizzaRepository.save(voucher7);
                pizzaRepository.save(voucher8);
                pizzaRepository.save(voucher9);
                pizzaRepository.save(voucher10);

        }

        public List<Voucher> getVoucher(String page, String size) {

                int pageNumber = Integer.parseInt(page);

                int pageSize = Integer.parseInt(size);

                Pageable pageableVoucher = PageRequest.of(pageNumber, pageSize);

                Page<Voucher> voucherPage = voucherRepository.findAll(pageableVoucher);
                List<Voucher> voucherList = voucherPage.getContent();
                return voucherList;
        }
}
