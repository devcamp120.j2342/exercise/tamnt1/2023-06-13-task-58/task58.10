package com.devcamp.voucher.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.voucher.models.Voucher;
import com.devcamp.voucher.services.VoucherService;

@RestController
@CrossOrigin
public class VoucherController {
    @Autowired
    private VoucherService voucherService;

    @GetMapping("/voucher")
    public ResponseEntity<List<Voucher>> getVoucherList(@RequestParam(value = "page", defaultValue = "0") String page,
            @RequestParam(value = "size", defaultValue = "5") String size) {

        List<Voucher> list = voucherService.getVoucher(page, size);
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @GetMapping("/create-voucher")
    public void createVoucher() {
        voucherService.createVouchers();
    }

}
