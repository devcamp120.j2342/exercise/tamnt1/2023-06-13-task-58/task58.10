package com.devcamp.voucher.models;

import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "voucher")
public class Voucher {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(length = 30)
    private String maVoucher;
    @Column(length = 30)
    private String phamTramGiamGia;
    @Column(length = 60)
    private String ghiChu;
    @Column(length = 30, nullable = false)
    private String ngayTao;

    @Column(length = 30)
    private String ngayCapNhat;

    public Voucher() {

    }

    public Voucher(String maVoucher, String phamTramGiamGia, String ghiChu, String ngayTao, String ngayCapNhat) {
        this.maVoucher = maVoucher;
        this.phamTramGiamGia = phamTramGiamGia;
        this.ghiChu = ghiChu;
        this.ngayTao = ngayTao;
        this.ngayCapNhat = ngayCapNhat;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMaVoucher() {
        return maVoucher;
    }

    public void setMaVoucher(String maVoucher) {
        this.maVoucher = maVoucher;
    }

    public String getPhamTramGiamGia() {
        return phamTramGiamGia;
    }

    public void setPhamTramGiamGia(String phamTramGiamGia) {
        this.phamTramGiamGia = phamTramGiamGia;
    }

    public String getGhiChu() {
        return ghiChu;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    public String getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(String ngayTao) {
        this.ngayTao = ngayTao;
    }

    public String getNgayCapNhat() {
        return ngayCapNhat;
    }

    public void setNgayCapNhat(String ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }

}
