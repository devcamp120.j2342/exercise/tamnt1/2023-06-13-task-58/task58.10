package com.devcamp.voucher.respository;

import org.springframework.data.repository.CrudRepository;

import com.devcamp.voucher.models.Voucher;

public interface PizzaRepository extends CrudRepository<Voucher, Integer> {

}
