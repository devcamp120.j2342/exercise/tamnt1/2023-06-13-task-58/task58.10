package com.devcamp.voucher.respository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.devcamp.voucher.models.Voucher;

public interface VoucherRepository extends PagingAndSortingRepository<Voucher, Integer> {

}
