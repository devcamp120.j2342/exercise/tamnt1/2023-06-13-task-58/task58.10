/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */

const gVOUCHER_URL = "http://localhost:8080/voucher";
const gCOLUMN_ID = {
    stt: 0,
    maVoucher: 1,
    phamTramGiamGia: 2,
    ghiChu: 3,
    ngayTao: 4,
    ngayCapNhat:5,

}
const gCOL_NAME = [
    "stt",
    "maVoucher",
    "phamTramGiamGia",
    "ghiChu",
    "ngayTao",
    "ngayCapNhat",

]
//Hàm chính để load html hiển thị ra bảng
class Main {
    constructor() {
        $(document).ready(() => {
            this.vOrderList = new RenderPage()
            this.vOrderList.renderPage()

        })
    }

}
new Main()
/*** REGION 2 - vùng để render ra bảng*/
class RenderPage {
    constructor() {
        this.vApi = new CallApi()

    }

    //Hàm gọi api lấy danh sách đơn hàng
    _getVoucherList() {
        this.vApi.onGetVoucherClick((paramVoucher) => {
            console.log(paramVoucher)
            this._createOrderTable(paramVoucher)
        })
    }

    //Hàm tạo các thành phần của bảng
    _createOrderTable(paramVoucher) {
        let stt = 1
        if ($.fn.DataTable.isDataTable('#table-order')) {
            $('#table-order').DataTable().destroy();
        }
        const vOrderTable = $("#table-order").DataTable({
            // Khai báo các cột của datatable
            "columns": [
                { "data": gCOL_NAME[gCOLUMN_ID.stt] },
                { "data": gCOL_NAME[gCOLUMN_ID.maVoucher] },
                { "data": gCOL_NAME[gCOLUMN_ID.phamTramGiamGia] },
                { "data": gCOL_NAME[gCOLUMN_ID.ghiChu] },
                { "data": gCOL_NAME[gCOLUMN_ID.ngayTao] },
                { "data": gCOL_NAME[gCOLUMN_ID.ngayCapNhat] },
        
            ],
            // Ghi đè nội dung của cột action
            "columnDefs": [
               
                {
                    targets: gCOLUMN_ID.stt,
                    render: function() {
                       return stt++
                    }
                }],

        });
        vOrderTable.clear() // xóa toàn bộ dữ liệu trong bảng
        vOrderTable.rows.add(paramVoucher) // cập nhật dữ liệu cho bảng
        vOrderTable.draw()// hàm vẻ lại bảng
    }

    // Hàm sẽ được gọi ở class Main 
    renderPage() {
        this._getVoucherList()
    }
}

/*** REGION 3 - vùng để gọi lên cơ sở dữ liệu lấy đa ta về*/
class CallApi {
    constructor() {

    }
    onGetVoucherClick(paramCallbackFn) {
     
        $.ajax({
            url: gVOUCHER_URL,
            method: "GET",
            success: function (data) {
                paramCallbackFn(data)
            },
            error: function (jqXHR, textStatus, errorThrown) {
               
            }
        });
    }

}